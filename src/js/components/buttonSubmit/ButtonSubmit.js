import React, { Component } from 'react';
import styles from '../../../styles/ButtonSubmit.css';

export default class ButtonSubmit extends Component {
    render() {
        return (
            <button type="submit" id="buttonTask"
                    className={styles.button + ' ' + styles.formButton + ' ' + styles.redButton}
                    style={this.props.style} disabled={this.props.isDisabled}
                    onClick={this.props.createTask}>
                {this.props.text}
            </button>
        )
    }
}