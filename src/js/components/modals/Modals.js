import React, {Component} from 'react';
import styles from '../../../styles/Modals.css';

export default class Modals extends Component {
    constructor(props) {
        super(props);
        this.getDate = this.getDate.bind(this);
    }

    getDate(date) {
        let yy = date.getFullYear() % 100,
            mm = date.getMonth() + 1,
            dd = date.getDate(),
            hh = date.getHours(),
            mi = date.getMinutes();

        if (yy < 10) yy = '0' + yy;
        if (mm < 10) mm = '0' + mm;
        if (dd < 10) dd = '0' + dd;
        return dd + '-' + mm + '-' + yy + ' в ' + hh + ':' + mi;
    }

    componentDidUpdate() {
        this.props.data.map(function(item) {
            if (!item.done) {
                let timeLeft = Math.round( (new Date(item.dateTill) - new Date()) / 1000 / 60 / 60 );
                if ( timeLeft <= item.remindMeIn ) {
                    this.props.showNotification();
                }
            }
        }, this);
    }

    componentDidMount() {
        this.props.data.map(function(item) {
            if (!item.done) {
                let timeLeft = Math.round( (new Date(item.dateTill) - new Date()) / 1000 / 60 / 60 );
                if ( timeLeft <= item.remindMeIn ) {
                    this.props.showNotification();
                }
            }
        }, this);
    }

    render() {
        return (
            <div>
                {this.props.data.map(function(item) {
                    let textButton = 'Завершить';
                    let styleButton = null;
                    let isDisabled = false;
                    if ( this.props.doneTasksList.some( t => t.id === item.id) ) {
                        textButton = 'Завершено';
                        styleButton = {opacity: '0.4', cursor: 'default'};
                        isDisabled = true;
                    }
                    let date = null;
                    if (item.dateTill === 'Бессрочно' || item.dateTill.length === 10) {
                        date = item.dateTill;
                    } else {
                        date = this.getDate( new Date(item.dateTill) )
                    }
                    return (
                        <div className={styles.modal} key={item.id} id={item.id}
                             style={this.props.openedModal === item.id
                                 ? {visibility: 'visible'}
                                 : {visibility: 'hidden'}}>
                            <div className={styles.overlay} onClick={this.props.closeModal} />
                            <div className={styles.visible}>
                                <h2>{item.title}</h2>
                                <div className={styles.groupAndDate}>
                                    <div className={styles.inline}>
                                        <p className={styles.category}>{item.category}</p>
                                        <p className={styles.subcategory}>{item.subcategory}</p>
                                    </div>
                                    <div className={styles.inline}>
                                        <p>До: <span className={styles.end}>
                                                 {date}
                                            </span>
                                        </p>
                                        <p className={styles.start}>От: {this.getDate( new Date(item.date) )}</p>
                                    </div>
                                </div>
                                <div className={styles.content}>
                                    {item.description}
                                </div>
                                <div className={styles.buttons}>
                                    <button type="button" onClick=
                                        {(e) => this.props.finishTask(
                                            Number(e.target.parentNode.parentNode.parentNode.id)
                                        )}
                                            style={styleButton} disabled={isDisabled}>
                                        {textButton}
                                    </button>
                                    <button type="button" onClick={this.props.closeModal}>Закрыть</button>
                                </div>
                            </div>
                        </div>
                    )
                }, this)}
            </div>
        )
    }
}