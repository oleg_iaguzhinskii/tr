import React, { Component } from 'react';
import styles from '../../../styles/ButtonReset.css';


export default class ButtonReset extends Component {
    render() {
        // onClick={this.props.resetAll}
        return (
            <button type="reset" className=
                {styles.button + ' ' + styles.formButton + ' ' + styles.blackButton}
                    style={{opacity: '0.2', cursor: 'default'}}>Отменить</button>
        )
    }
}