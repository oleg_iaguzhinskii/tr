import React, {Component} from 'react';
import styles from '../../../styles/Footer.css';

export default class Footer extends Component {
    render() {
        return (
            <div className={styles.footer}>
                <p>Олег Назаров | 2017</p>
            </div>
        )
    }
}