import React, { Component } from 'react';
import styles from '../../../styles/Creation.css';

export default class InputTitle extends Component {
    render() {
        return (
            <div className={styles.tableRow}>
                <label htmlFor="input1">
                    <p>Название</p>
                </label>
                <p>
                    <input type="text" className={styles.input1}
                           onChange={e => this.props.setTextTitle(e.target.value)} id="input1"/>
                </p>
            </div>
        )
    }
}