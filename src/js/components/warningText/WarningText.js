import React, { Component } from 'react';
import styles from '../../../styles/WarningText.css';

export default class WarningText extends Component {
    render() {
        return (
            <p className={styles.warning} style={this.props.style}>
                {this.props.text}
            </p>
        )
    }
}
