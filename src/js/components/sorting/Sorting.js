import React, {Component} from 'react';
import styles from '../../../styles/Sorting.css';

class Sorting extends Component {
    render() {
        var sorting = [
            {
                className: 'date',
                val: 'По дате',
                id: 1
            },
            {
                className: 'tag',
                val: 'По важности',
                id: 2
            },
            {
                className: 'done',
                val: 'По выполнению',
                id: 3
            }
        ];
        return (
            <div className={styles.sort}>
                <p>Сортировка:</p>
                {sorting.map(function(item) {
                    var text = item.val;
                    if (this.props.sortBy === item.className) {
                        text += this.props.descending ? ' \u2191' : ' \u2193'
                    }
                   return (
                       <p className={item.className}
                          onClick={e => this.props.sort(e.target.classList.value)} key={item.id}>
                           {text}
                       </p>
                   )
                }, this)}
            </div>
        )
    }
}

export default Sorting;