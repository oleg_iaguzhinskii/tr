import React, { Component } from 'react';
import styles from '../../../styles/Creation.css';

export default class SelectSubCategory extends Component {
    render() {
        return (
            <div className={styles.tableRow}>
                <label htmlFor="select2">
                    <p>Подкатегория</p>
                </label>
                <p>
                    <select className={styles.select1} id="select2"
                            onChange={ e => this.props.selectSubcategory(e.target.value)}>
                        {this.props.subcategoriesList.map(function (item) {
                            return (
                                <option key={item.id}>{item.title}</option>
                            )
                        })}
                    </select>
                </p>
            </div>
        )
    }
}