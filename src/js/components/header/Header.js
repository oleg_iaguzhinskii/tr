import React, {Component} from 'react';
import styles from '../../../styles/Header.css';

class Header extends Component {
    render() {
        let notificationStyle = this.props.hasImmediateTask ? {visibility: 'visible'} : {visibility: 'hidden'};
        let checkedPage = this.props.route.slice(1);
        let color1 = null,
            color2 = null,
            color3 = null;
        switch (checkedPage) {
            case 'index':
                color1 = {backgroundColor: 'red'};
                break;
            case 'creation':
                color2 = {backgroundColor: 'red'};
                break;
            case 'groups':
                color3 = {backgroundColor: 'red'};
                break;
            default:
                color1 = {backgroundColor: 'red'};
        }
        return (
            <div className="Header">
                <div className={styles.nickname}>
                    <p>Добрый день, <span className={styles.login}>
                        <span className={styles.circle} style={notificationStyle}
                              data-title="Пора выполнить задание!" />
                        {this.props.userLogin}</span>!</p>
                    <p className={styles.quit} onClick={this.props.logOut}>ВЫХОД</p>
                </div>
                <nav className={styles.nav}>
                    <a href="#/index" className={styles.button + ' ' + styles.blackButton}
                       style={color1}>Все задачи</a>
                    <a href="#/creation" className={styles.button + ' ' + styles.blackButton}
                       style={color2}>Создать</a>
                    <a href="#/groups" className={styles.button + ' ' + styles.blackButton}
                       style={color3}>Группы</a>
                </nav>
            </div>
        )
    }
}

export default Header;