import React, { Component } from 'react';
import styles from '../../../styles/Form.css';
import WarningText from "../warningText/WarningText";

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            typedLogin: '',
            typedPassword: '',
            isWarningVisible: false,
            isButtonDisabled: false
        };

        this.handleLoginChange = this.handleLoginChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.checkLoginAndPassword = this.checkLoginAndPassword.bind(this);
    }

    handleLoginChange(event) {
        var text = event.target.value.toLowerCase();
        if (text.search( /[^0-9a-z]/i ) !== -1) {
            this.setState({
                isWarningVisible: true,
                isButtonDisabled: true,
                typedLogin: text
            });
        } else {
            this.setState({
                isWarningVisible: false,
                isButtonDisabled: false,
                typedLogin: text
            });
        }
    }

    handlePasswordChange(event) {
        this.setState({typedPassword: event.target.value})
    }

    checkLoginAndPassword() {
        var userLogin = this.props.usersList.filter( function(u) {
            return this.state.typedLogin === u.login;
        }, this)[0];

        if (userLogin) {
            if (userLogin.password === this.state.typedPassword) {
                this.props.logIn(userLogin.id);
                this.setState({
                    typedLogin: '',
                    typedPassword: ''
                })
            } else {
                console.log('неправильный пароль');
            }
        } else {
            console.log('неправильный логин');
        }
    }

    render() {
        let visibility = this.state.isWarningVisible
                ? {visibility: 'visible', fontSize: '0.5em'}
                : {visibility: 'hidden', fontSize: '0.5em'};
        return (
            <div className={styles.windowLoginForm} style={this.props.style}>
                <div className={styles.overlayLoginForm}/>
                <div className={styles.loginForm}>
                    <h3>Форма входа</h3>
                    <input type="text" placeholder="Логин (admin)" value={this.state.typedLogin}
                           onChange={this.handleLoginChange} />
                    <WarningText style={visibility}
                                 text={'Поле логин должно состоять только из чисел или латинских символов'}/>
                    <input type="password" placeholder="Пароль (admin)" value={this.state.typedPassword}
                           onChange={this.handlePasswordChange} />
                    <input type="submit" value="Войти" disabled={this.state.isButtonDisabled}
                           onClick={this.checkLoginAndPassword} />
                </div>
            </div>
        )
    }
}

export default Form;