import React, {Component} from 'react';
import styles from '../../../styles/Tasks.css';
import Img from 'react-image';

export default class Tasks extends Component {
    render() {
        return (
            <div>
                {this.props.data.map(function(item) {
                    let color = '';
                    switch (item.tag) {
                        case 1: color = 'red';
                        break;
                        case 2: color = 'green';
                        break;
                        case 3: color = 'yellow';
                        break;
                        default: color = 'inherit';
                    }
                    let isOpacity = this.props.doneTasksList.some( t => t.id === item.id )
                        ? {opacity: '0.4'} : null;
                    return (
                        <div className={styles.task} key={item.id} id={item.id}
                            style={isOpacity}
                            onClick={(e) => this.props.showModal( Number(e.currentTarget.id) )}>
                            <Img className={styles.smallImg} src=
                                {'http://cdn1.savepice.ru/uploads/2017/8/1/3c1552e58ac7137efcc222e24ee08d83-full.png'}/>
                            <p className={styles.smallDescription} style={{backgroundColor: color}}>
                                {item.title}
                                <br />
                                <i>{item.description}</i>
                            </p>
                        </div>
                    )
                }, this)}
            </div>
        )
    }
}