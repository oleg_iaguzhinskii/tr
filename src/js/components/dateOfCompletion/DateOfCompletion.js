import React, { Component } from 'react';
import styles from '../../../styles/Creation.css';

export default class InputTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            today: null
        }
    }
    componentWillMount() {
        let today = new Date();

        let yy = today.getFullYear();
        let mm = today.getMonth() + 1;
        let dd = today.getDate();

        if (yy < 10) yy = '0' + yy;
        if (mm < 10) mm = '0' + mm;
        if (dd < 10) dd = '0' + dd;

        this.setState({
            today: '' + yy + '-' + mm + '-' + dd
        });
    }

    render() {
        return (
            <div className={styles.tableRow}>
                <label htmlFor="dateTill">
                    <p>Дата завершения</p>
                </label>
                <p>
                    <input type="date" className={styles.dateTill} id="dateTill" min={this.state.today}
                        onChange={ e => this.props.selectDateTill(e.target.value)}/>
                </p>
            </div>
        )
    }
}