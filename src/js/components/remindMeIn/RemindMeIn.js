import React, { Component } from 'react';
import styles from '../../../styles/Creation.css';

export default class RemindMeIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hoursToRemind: 12,
            text: 'часов'
        };
        this.setHoursToRemind = this.setHoursToRemind.bind(this);
    }

    componentDidMount() {
        this.props.setRemindMeIn(this.state.hoursToRemind);
    }

    setHoursToRemind(hours) {
        let text = '';
        switch ( Number(hours) ) {
            case 1:
            case 21:
            case 31:
            case 41:
                text = 'час';
                break;
            case 2:
            case 3:
            case 4:
            case 22:
            case 23:
            case 24:
            case 32:
            case 33:
            case 34:
            case 42:
            case 43:
            case 44:
                text = 'часа';
                break;
            default:
                text = 'часов';
                break;
        }
        this.setState({
            hoursToRemind: hours,
            text: text
        });
        this.props.setRemindMeIn(this.state.hoursToRemind);
    }

    render() {
        return (
            <div className={styles.tableRow}>
                <label htmlFor="remindMeIn">
                    <p>Напомнить за</p>
                </label>
                <p>
                    <span className={styles.spanHours}>{this.state.hoursToRemind}</span>
                    <span className={styles.spanText}>{this.state.text}</span>
                    <input type="range" className={styles.remindMeIn}  id="remindMeIn"
                           value={this.state.hoursToRemind} min="1" max="48"
                                                      onChange={ e => this.setHoursToRemind(e.target.value)}/>
                </p>
            </div>
        )
    }
}