import React, { Component } from 'react';
import styles from '../../../styles/Creation.css';

export default class Textarea extends Component {
    constructor(props) {
        super(props);
        this.typeText = this.typeText.bind(this);
    }

    typeText(text) {
        let checkedTag = text.toLowerCase().match(/\[.*\]/);
        if(checkedTag) {
            switch( checkedTag[0].slice(1, -1).replace(/ё/, 'е') ) {
                case 'красный':
                    checkedTag = 1;
                    break;
                case 'зеленый':
                    checkedTag = 2;
                    break;
                case 'желтый':
                    checkedTag = 3;
                    break;
                default:
                    checkedTag = 0;
            }
        } else {
            checkedTag = 4;
        }
        this.props.setTextTextarea(text, checkedTag);
    }

    render() {
        return (
            <div className={styles.tableRow}>
                <label className={styles.label} htmlFor="textarea1">
                    <p>Описание</p>
                </label>
                <p>
                    <textarea value={this.props.textTextarea} className={styles.textarea1} id="textarea1"
                        onChange={e => this.typeText(e.target.value)}/>
                </p>
            </div>
        )
    }
}