import React, { Component } from 'react';
import InputTitle from '../../components/inputTitle/InputTitle';
import SelectCategory from '../../components/selectCategory/SelectCategory';
import SelectSubCategory from '../../components/selectSubCategory/SelectSubCategory';
import DateOfCompletion from '../../components/dateOfCompletion/DateOfCompletion';
import RemindMeIn from '../../components/remindMeIn/RemindMeIn';
import Textarea from '../../components/textarea/Textarea';
import WarningText from '../../components/warningText/WarningText';
import ButtonReset from "../../components/buttonReset/ButtonReset";
import ButtonSubmit from "../../components/buttonSubmit/ButtonSubmit";
import styles from '../../../styles/Creation.css';

export default class Creation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCategory: this.props.categoriesList[0].title || '',
            selectedSubcategory: this.props.categoriesList[0].subcategories[0].title || '',
            subcategoriesList: this.props.categoriesList[0].subcategories || [],
            selectedDateTill: null,
            remindMeIn: null,
            textTextarea: '',
            textTitle: '',
            tag: 4,
            isFieldEmpty: false
        };
        this.insertSubcategories = this.insertSubcategories.bind(this);
        this.selectSubcategory = this.selectSubcategory.bind(this);
        this.selectDateTill = this.selectDateTill.bind(this);
        this.setRemindMeIn = this.setRemindMeIn.bind(this);
        this.setTextTextarea = this.setTextTextarea.bind(this);
        this.setTextTitle = this.setTextTitle.bind(this);
        // this.resetAll = this.resetAll.bind(this);
        this.createTask = this.createTask.bind(this);
        this.autoTextEdit = this.autoTextEdit.bind(this);
    }

    insertSubcategories(category) {
        let cat = this.props.categoriesList.filter( c => c.title === category)[0];
        this.setState({
            selectedCategory: cat.title,
            selectedSubcategory: cat.subcategories[0].title,
            subcategoriesList: cat.subcategories
        })
    }

    selectSubcategory(subcategory) {
        this.setState({
            selectedSubcategory: subcategory
        })
    }

    selectDateTill(date) {
        if (date.length > 0) {
            date = date.split('-');
            date = date.reverse().join('-');
            this.setState({
                selectedDateTill: date
            });
        } else {
            this.setState({
                selectedDateTill: null
            });
        }
    }

    setRemindMeIn(hours) {
        this.setState({
            remindMeIn: hours
        })
    }

    setTextTextarea(text, tag) {
        this.setState({
            textTextarea: text,
            tag: tag,
            isFieldEmpty: false
        })
    }

    setTextTitle(text) {
        this.setState({
            textTitle: text,
            isFieldEmpty: false
        })
    }

    // resetAll() {
    //     this.setState({
    //         selectedCategory: this.props.categoriesList[0].title || '',
    //         selectedSubcategory: this.props.categoriesList[0].subcategories[0].title || '',
    //         subcategoriesList: this.props.categoriesList[0].subcategories || [],
    //         selectedDateTill: null,
    //         remindMeIn: null,
    //         textTextarea: ''
    //     });
    // }

    createTask() {
        let self = this;
        let textarea = this.autoTextEdit(this.state.textTextarea);
        let title = this.autoTextEdit(this.state.textTitle);
        let dateTill = this.state.selectedDateTill ? this.state.selectedDateTill : 'Бессрочно';
        if (textarea === undefined || title === undefined)
            return;
        let obj = {
            "login": this.props.currentUserLogin,
            "title": title,
            "date": new Date(),
            "dateTill": dateTill,
            "remindMeIn": this.state.remindMeIn,
            "category": this.state.selectedCategory,
            "subcategory": this.state.selectedSubcategory,
            "description": textarea,
            "tag": this.state.tag,
            "done": false,
            "id": this.props.lastTaskId + 1
        };

        fetch('http://localhost:3000/tasks/', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
            .then(function(response) {
                self.props.updateTasksList(obj);

                return response.json()
            })
            .catch(function(ex) {
                console.log('parsing failed: ', ex)
            });
    }

    autoTextEdit(fieldValue) {
        let sentences = [];
        let text = fieldValue.replace(/\[.+\]/, '');
        if (text.match(/\S/) === null) {
            this.setState({
                isFieldEmpty: true
            });
            return;
        }
        text = text.replace(/^\s+/, '').replace(/\s+$/, '')
            .replace(/\s{2,}/g, ' ');
        sentences = text.toLowerCase().match(/[^.]+\.?( *|$)/g);
        sentences = sentences.map(e => e.charAt(0).toUpperCase() + e.slice(1));
        text = sentences.join('');

        return text;
    }

    render() {
        let text = 'Заполните, пожалуйста, корректно';
        let styleWarning = {visibility: 'hidden'};
        let styleSubmit = {};
        let isDisabled = false;
        if (this.state.tag === 0) {
            styleWarning = {
                visibility: 'visible',
                color: 'red',
                fontSize: '0.9em'
            };
            styleSubmit = {
                backgroundColor: '#EEE8AA',
                cursor: 'default'
            };
            isDisabled = true;
            text = 'Неправильный тег!';
        } else if (this.state.isFieldEmpty) {
            styleWarning = {
                visibility: 'visible',
                color: 'red',
                fontSize: '0.9em'
            };
            styleSubmit = {
                backgroundColor: '#EEE8AA',
                cursor: 'default'
            };
            isDisabled = true;
            text = 'Поля не могут быть пустыми!';
        }
        return (
            <div className={styles.form}>
                <InputTitle setTextTitle={this.setTextTitle}/>
                <SelectCategory categoriesList={this.props.categoriesList}
                                insertSubcategories={this.insertSubcategories}/>
                <SelectSubCategory subcategoriesList={this.state.subcategoriesList}
                                    selectSubcategory={this.selectSubcategory}/>
                <DateOfCompletion selectDateTill={this.selectDateTill}/>
                <RemindMeIn setRemindMeIn={this.setRemindMeIn}/>
                <Textarea value={this.state.textTextarea} setTextTextarea={this.setTextTextarea}/>
                <div className={styles.tableRow}>
                    <p/>
                    <WarningText text={text} style={styleWarning}/>
                </div>
                <div className={styles.tableRow}>
                    <p/>
                    <p>
                        <ButtonReset resetAll={this.resetAll}/>
                        <ButtonSubmit text={'Создать'} style={styleSubmit} isDisabled={isDisabled}
                                      createTask={this.createTask}/>
                    </p>
                </div>
            </div>
        )
    }
}
