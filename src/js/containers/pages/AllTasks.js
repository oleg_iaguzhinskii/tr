import React, { Component } from 'react';
import Sorting from '../../components/sorting/Sorting';
import Tasks from '../../components/tasks/Tasks';
import Modals from '../../components/modals/Modals';

class AllTasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openedModal: null,
            dataTasks: props.tasksList,
            sortBy: null,
            descending: false
        };
        this.showModal = this.showModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.finishTask = this.finishTask.bind(this);
        this.sort = this.sort.bind(this);
    }

    componentWillReceiveProps(newProps) {
        this.setState({dataTasks: newProps.tasksList});
    }

    showModal(modalId) {
        this.setState({openedModal: modalId});
    }

    closeModal() {
        this.setState({openedModal: null});
    }

    sort(sortBy) {
        let column = sortBy;
        let descending = this.state.sortBy === column && !this.state.descending
        let data = this.state.dataTasks;
        data.sort(function(a, b) {
            return descending
                    ? a[column] < b[column]
                    : a[column] > b[column]
        });
        this.setState({
            dataTasks: data,
            sortBy: column,
            descending: descending
        });
    }

    finishTask(taskId) {
        let self = this;
        let currentTask = this.state.dataTasks.filter( t => t.id === taskId)[0];
        fetch('http://localhost:3000/tasks/' + currentTask.id, {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "login": currentTask.login,
                "title": currentTask.title,
                "date": currentTask.date,
                "dateTill": currentTask.dateTill,
                "remindMeIn": currentTask.remindMeIn,
                "category": currentTask.category,
                "subcategory": currentTask.subcategory,
                "description": currentTask.description,
                "tag": currentTask.tag,
                "done": true,
                "id": currentTask.id
            })
        })
            .then(function(response) {
                if ( !currentTask.done ) {
                    let temp = self.state.dataTasks;
                    let ind = temp.indexOf(currentTask);
                    currentTask.done = true;
                    temp.splice(ind, 1, currentTask);
                    self.setState({
                        openedModal: null,
                        dataTasks: temp
                    });
                }
                self.props.shouldHideNotification();
                return response.json();
            })
            .catch(function(ex) {
                console.log('parsing failed: ' +ex);
            });
    }

    render() {
        let dataTasks = this.state.dataTasks;
        let doneTasksList = dataTasks.filter( t => t.done === true);
        return (
            <div>
                <Sorting sort={this.sort}
                         sortBy={this.state.sortBy}
                         descending={this.state.descending} />
                <Tasks data={dataTasks}
                       showModal={this.showModal}
                       doneTasksList={doneTasksList} />
                <Modals data={dataTasks}
                        openedModal={this.state.openedModal}
                        doneTasksList={doneTasksList}
                        closeModal={this.closeModal}
                        finishTask={this.finishTask}
                        showNotification={this.props.showNotification} />
            </div>
        )
    }
}

export default AllTasks;