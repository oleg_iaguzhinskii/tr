import React, { Component } from 'react';
import Form from '../components/form/Form';
import Header from '../components/header/Header';
import AllTasks from './pages/AllTasks';
import Creation from './pages/Creation';
import Footer from '../components/footer/Footer';
import styles from '../../styles/main.css';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            route: window.location.hash.substr(1),
            isLogged: props.usersList.filter( u => u.switch === 'on').length > 0,
            currentUser: props.usersList.filter( u => u.switch === 'on')[0] || '',
            hasImmediateTask: false,
            tasksList: this.props.tasksList,
            lastTaskId: this.props.tasksList[this.props.tasksList.length - 1].id
        };
        this.logIn = this.logIn.bind(this);
        this.logOut = this.logOut.bind(this);
        this.showNotification = this.showNotification.bind(this);
        this.shouldHideNotification = this.shouldHideNotification.bind(this);
        this.selectTasksCurrentUser = this.selectTasksCurrentUser.bind(this);
        this.updateTasksList = this.updateTasksList.bind(this);
    }
    componentDidMount() {
        window.addEventListener('hashchange', () => {
            this.setState({
                route: window.location.hash.substr(1)
            })
        })
    }

    selectTasksCurrentUser(tasks) {
        return tasks.filter( t => t.login === this.state.currentUser.login);
    }

    logIn(userId) {
        let self = this;
        let currentUser = this.props.usersList.filter( u => u.id, this)[0];
        fetch('http://localhost:3000/users/' + userId, {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "login": currentUser.login,
                "password": currentUser.password,
                "switch": "on",
                "id": currentUser.id
            })
        })
            .then(function(response) {
                currentUser.switch = "on";
                self.setState({
                    isLogged: true,
                    currentUser: currentUser
                });
                return response.json();
            })
            .catch(function(ex) {
                console.log('parsing failed: ' +ex);
            });
    }

    logOut() {
        let self = this;
        let currentUser = this.state.currentUser;
        fetch('http://localhost:3000/users/' + currentUser.id, {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "login": currentUser.login,
                "password": currentUser.password,
                "switch": "off",
                "id": currentUser.id
            })
        })
            .then(function(response) {
                self.setState({
                    isLogged: false,
                    currentUser: ''
                });
                return response.json();
            })
            .catch(function(ex) {
                console.log('parsing failed: ' +ex);
            });
    }

    showNotification() {
        if (!this.state.hasImmediateTask) {
            this.setState({hasImmediateTask: true});
        }
    }

    shouldHideNotification() {
        if (this.props.tasksList.filter(t => t.login === this.state.currentUser.login).every(t => t.done === true)) {
            this.setState({hasImmediateTask: false});
        }
    }

    updateTasksList(task) {
        let obj = this.state.tasksList;
        let lastTaskId = this.state.lastTaskId;
        obj.push(task);
        this.setState({
            tasksList: obj,
            lastTaskId: lastTaskId + 1
        });
    }

    render() {
        let Child;
        let prop1 = null,
            prop2 = null,
            prop3 = null,
            prop4 = null,
            prop5 = null,
            prop6 = null,
            prop7 = null;
        let tasksList = this.selectTasksCurrentUser(this.props.tasksList),
            showNotification = this.showNotification,
            shouldHideNotification = this.shouldHideNotification,
            categoriesList = this.props.categoriesList;

        let currentUserLogin = this.state.currentUser ?  this.state.currentUser.login : '';

        switch (this.state.route) {
            case '/index':
                Child = AllTasks;

                prop1 = tasksList;
                prop2 = showNotification;
                prop3 = shouldHideNotification;
                break;
            case '/creation':
                Child = Creation;

                prop4 = categoriesList;
                prop5 = currentUserLogin;
                prop6 = this.updateTasksList;
                prop7 = this.state.lastTaskId;
                break;
            default:
                Child = AllTasks;

                prop1 = tasksList;
                prop2 = showNotification;
                prop3 = shouldHideNotification;
        }

        return (
            <div className={styles.body}>
                <Form style={this.state.isLogged ? {visibility: 'hidden'} : null}
                      usersList={this.props.usersList}
                      logIn={this.logIn} />
                <Header userLogin={this.state.currentUser ? this.state.currentUser.login : ''}
                        logOut={this.logOut}
                        hasImmediateTask={this.state.hasImmediateTask}
                        route={this.state.route} />
                <Child tasksList={prop1}
                       showNotification={prop2}
                       shouldHideNotification={prop3}
                       categoriesList={prop4}
                       currentUserLogin={prop5}
                       updateTasksList={prop6}
                       lastTaskId={prop7}
                />
                <Footer />
            </div>
        )

    }
}