import React from 'react';
import { render } from 'react-dom';
// import AllTasks from './js/containers/AllTasks';
import App from './js/containers/App';
// import { Router, Route, IndexRoute, browserHistory } from 'react-router';

fetch('http://localhost:3000/users')
    .then(function(response) {
        return response.json();
    })
    .then(function(users) {

        fetch('http://localhost:3000/category')
            .then(function(response) {
                return response.json();
            })
            .then(function(category) {

                fetch('http://localhost:3000/tasks')
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(tasks) {

                        render(
                            <App usersList={users} categoriesList={category} tasksList={tasks} />,
                            document.getElementById('root')
                        )

                    })
                    .catch(function(ex) {
                        console.log('parsing failed: ' + ex);
                    });

            })
            .catch(function(ex) {
                console.log('parsing failed: ' + ex);
            });

    })
    .catch(function(ex) {
        console.log('parsing failed: ' + ex);
    });